//
//  GetJSON.h
//  SampleJSOn
//
//  Created by Akhila Rao on 5/6/13.
//  Copyright (c) 2013 Testing. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GetJSON : NSObject <NSURLConnectionDataDelegate>
{
    
}

@property (nonatomic, strong) NSMutableData *receivedData;
- (void) getJSON;
@end
