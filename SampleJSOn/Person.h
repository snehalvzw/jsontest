//
//  Person.h
//  SampleJSOn
//
//  Created by Akhila Rao on 5/6/13.
//  Copyright (c) 2013 Testing. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) int idNumber;

@end
