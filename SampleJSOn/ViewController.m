//
//  ViewController.m
//  SampleJSOn
//
//  Created by Akhila Rao on 5/6/13.
//  Copyright (c) 2013 Testing. All rights reserved.
//

#import "ViewController.h"
#import "GetJSON.h"
#import "Person.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(methodOnReceivingJSONNotification:) name:@"JSON_READ_NOTIFICATION" object:nil];
    
    GetJSON *getJSONObject = [[GetJSON alloc] init];
    
    [getJSONObject getJSON];
}

//method will be called on receiving notification , notificatino is posted from GetJSON class

- (void) methodOnReceivingJSONNotification:(NSNotification *)notification
{
    NSLog(@"Yay!! JSON read ");
    self.fetchedArray = [notification object];
    
//    for (Person *p in self.fetchedArray)
//    {
//        NSLog(@"Person name %@ %i", p.name , p.idNumber );
//    }
    personTableView.delegate = self;
    personTableView.dataSource = self;
    
    [personTableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (int)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  [self.fetchedArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        Person *p = [self.fetchedArray objectAtIndex:indexPath.row];
        cell.textLabel.text = [NSString stringWithFormat:@"%i   %@",p.idNumber, p.name];
    }
    
    return cell;
}

@end
