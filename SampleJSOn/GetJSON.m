//
//  GetJSON.m
//  SampleJSOn
//
//  Created by Akhila Rao on 5/6/13.
//  Copyright (c) 2013 Testing. All rights reserved.
//

#import "GetJSON.h"
#import "Person.h"

@implementation GetJSON

@synthesize receivedData;

static NSString *urlStringToRead = @"http://bismarck.sdsu.edu/photoserver/userlist";

- (void) getJSON
{
    NSURL *urlToRead = [NSURL URLWithString:urlStringToRead];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:urlToRead];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [connection start];
}

#pragma NSUrlConnectionDelegate

- (void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    NSLog(@"Connection did recieve response");
    self.receivedData = [NSMutableData data];
}

- (void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    NSLog(@"connectin did receive data %@ ",data);
    
    [self.receivedData appendData:data];
    
    NSError *jsonParsingError = nil;
    id object = [NSJSONSerialization JSONObjectWithData:self.receivedData options:0 error:&jsonParsingError];
    
    NSLog(@"object is %@ ",object);
    
}

- (void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"connection did fail with error %@ ",[error description]);
}

- (void) connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSLog(@"connection did finish loading");
    
    NSError *jsonParsingError = nil;
    id object = [NSJSONSerialization JSONObjectWithData:self.receivedData options:0 error:&jsonParsingError];
    
    if (jsonParsingError) {
        NSLog(@"JSON ERROR: %@", [jsonParsingError localizedDescription]);
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"JSON_READ_NOTIFICATION" object:nil];
    } else {
        //NSLog(@"OBJECT: %@",object);
        
        NSMutableArray *array = (NSMutableArray *)object;
        
        NSMutableArray *arrayToReturn = [NSMutableArray array];
        
        for(id obj in array)
        {
            Person *p = [[Person alloc] init];
            p.name = [obj valueForKey:@"name"];
            
            p.idNumber = [[obj valueForKey:@"id"] integerValue];
            
            [arrayToReturn addObject:p];
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:@"JSON_READ_NOTIFICATION" object:arrayToReturn];
    }
    
    
    
}


@end
