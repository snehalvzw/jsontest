//
//  ViewController.h
//  SampleJSOn
//
//  Created by Akhila Rao on 5/6/13.
//  Copyright (c) 2013 Testing. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
    IBOutlet UITableView *personTableView;
}
@property (nonatomic, strong) NSMutableArray *fetchedArray;

@end
